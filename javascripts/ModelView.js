/*

USAGE
-----
var ItemList = ModelView.extend({
  templateName: 'dashboard', // *required
  init: function(params){}, // *recommended | there is smth called params here
  initView: function(){}, // *recommended
  initDOM: function(){}, // *recommended
  updateView: function(){}, // *recommended
  
  ... ANY CUSTOM FUNCTION ...
});

var list = ItemList.create({
  name: "Duong Nguyen List"
});


OBJECT INIT LIFE CYCLE
----------------------
[START] init > initFunctions > addPrivateFunctions > addCustomFunctions > privateInit >
> initView > privateInitView > updateView > initDOM > privateInitDOM > privateUpdateView


METHOD INDEX
------------
. init
. initView
. initDOM
. bindView
. updateView
. render
. registerDOM
. initFunctions
. addPrivateFunctions
. addCustomFunctions

*/

window.ModelView = {
  extend: function(klassParams){
    var ModelViewClass = function(instanceParams){
      var klass = this,
          $dom,
          _privateFnNames,
          _domCollection = {};

      _privateFnNames = ['init', 'initView', 'initDOM', 'bindView', 'updateView', 'render'];

      this.init = function(){
        this.initFunctions();

        /*
        params inserted to privateInit, which is the init function declared in 'extend' function
        */
        this.privateInit(instanceParams || {});
        this.initView();
      };

      this.initView = function(){
        var template = Template.get(klassParams.templateName);
        $dom = $(template);

        this.registerDOM();
        this.privateInitView();
        this.updateView();
      };

      this.initDOM = function(){
        /*
        DOM Manipulation
        */
        this.privateInitDOM();
      };

      this.bindView = function(){
        /*
        Data binding
        */
        this.privateBindView();
      };

      this.updateView = function(){
        /*
        Extract data binding
        */
        this.initDOM();
        this.privateUpdateView();
      };

      this.render = function(){
        this.initDOM();
        this.privateRender();
        return $dom;
      };

      this.registerDOM = function(){
        var self = this;

        $dom.find("[bind-data]").each(function(){
          var key = $(this).attr("bind-data");
          self["$" + key] = $(this);
        });
      };

      this.initFunctions = function(){
        this.addPrivateFunctions();
        this.addCustomFunctions();
      };

      this.addPrivateFunctions = function(){
        /*
        Declare private functions - those to be triggered in pre-defined functions
        */
        _privateFnNames.forEach(function(fnName){
          klass["private" + fnName.capitalize()] = klassParams[fnName] || new Function;
        });
      };

      this.addCustomFunctions = function(){
        /*
        Additional Functions
        */
        for (var fnName in klassParams){
          if (klassParams.hasOwnProperty(fnName)){
            if (_privateFnNames.indexOf(fnName) == -1){
              klass[fnName] = klassParams[fnName];
            }
          }
        }
      };

      this.init();
    };

    ModelViewClass.create = function(instanceParams){
      return new ModelViewClass(instanceParams);
    };

    return ModelViewClass;
  }
};