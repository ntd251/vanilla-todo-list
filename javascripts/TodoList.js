window.TodoList = ModelView.extend({
  templateName: 'todo-list',

  init: function(params){
    this.name = params.name || "";
    this.isEditing = false;
    this.items = [];
  },

  initDOM: function(){
    var list = this;
    
    list.$btnAddItem.off('click').click(function(){
      list.$formAddItem.show();
    });

    list.$btnDelete.off('click').click(function(){
      list.remove();
    });

    list.$name.off('click').click(function(){
      list.edit();
    });

    list.$formEditList.off('submit').submit(function(e){
      e.preventDefault();
      list.save();
    });

    list.$formAddItem.off('submit').submit(function(e){
      e.preventDefault();
      list.addItem(list.$inputItemDescription.val());
      list.$inputItemDescription.val("");
    });
  },

  updateView: function(){
    var list = this;

    list.$name.html(this.name);

    // reset body content
    list.$items.html("");

    list.items.forEach(function(item){
      list.$items.append(item.render());
    });

    // Update editing
    if (list.isEditing){
      list.$editing.show();
      list.$notEditing.hide();
    } else {
      list.$editing.hide();
      list.$notEditing.show();
    }

    // update form content
    list.$inputName.val(this.name);
  },

  /*
  Private actions
  */

  setDashboard: function(dashboard){
    this.dashboard = dashboard;
  },

  addItem: function(description){
    var item = TodoItem.create({
      description: description
    });

    this.items.push(item);
    item.setList(this);
    this.updateView();
    return item;
  },

  removeItem: function(item){
    var index = this.items.indexOf(item);
    if (index > -1){
      this.items.splice(index, 1);
      this.updateView();
    }
  },

  updateName: function(name){
    this.name = name || "";
  },

  remove: function(){
    this.dashboard.removeList(this);
  },

  /*
  DOM
  */

  edit: function(){
    this.isEditing = true;
    this.updateView();
  },

  save: function(){
    this.isEditing = false;
    this.updateName($inputName.val());
    this.updateView();
  }
});