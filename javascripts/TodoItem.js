window.TodoItem = ModelView.extend({
  templateName: 'todo-item',

  init: function(params){
    this.description = params.description;
    this.isCompleted = false; 
  },

  initDOM: function(){
    var self = this;

    this.$checkbox.off('change').change(function(){
      self.toggle();
    });

    this.$btnDelete.off('click').click(function(){
      self.remove();
    }); 
  },

  updateView: function(){
    this.$checkbox.val(this.isCompleted);
    this.$label.html(this.description);
  },

  toggle: function(){
    this.isCompleted = !this.isCompleted;
  },

  remove: function(){
    this.list.removeItem(this);
  },
  
  setList: function(list){
    this.list = list;
  }
});