/*
Test timeframe: 2 hours

What we are testing your experties in
. HTML
. CSS
. Vanilla JavaScript and JavaScript Frameworks
. Object-oriented Programming

Your test is to implement a to-do list dashboard according to the provided design and instructions. 
All red color elements in the design are only for explanation purpose.

The dashboard has many to-do lists. Each to-do list has many items. The lists have fixed height, and are arranged inline.
Lists are sorted by name in alphanumeric order.

Action requirements
. Create a list
. Edit name of a list
. Remove a list
. Create an item
. Check an item as completed
. Uncheck an item as open
. Drag an item from a list to another
. Remove an item

Additional Requirement
. Implement function window.getOpenItems() to return an array of remaining items grouped by list.
E.g.
[
  {
    name: "Movies to watch",
    openItems: ["Interstella", "DogCat"]
  },
  {
    name: "Yooooo",
    openItems: []
  }
]

Your code should be
. Clean
. Scalable
. Testable
. Commented (if any)
*/

var dashboard = Dashboard.create();
$("#my-dashboard").append(dashboard.render());

window.getOpenItems = function(){
  return dashboard.getOpenItems();
};