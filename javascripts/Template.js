window.Template = {
  get: function(templateName){
    var dom = $('template[name="' + templateName + '"]').eq(0);
    return dom ? dom.html() : undefined;
  },

  list: function(){
    return $('template').map(function() { return $(this).attr('name'); });
  }
};