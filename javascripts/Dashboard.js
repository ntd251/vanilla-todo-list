window.Dashboard = ModelView.extend({
  templateName: 'dashboard',

  init: function(){
    this.lists = [];
  },

  initDOM: function(){
    var dashboard = this;
    
    dashboard.$formAddList.off('submit').submit(function(e){
      e.preventDefault();
      dashboard.addList(dashboard.$inputListName.val());
      dashboard.$inputListName.val("");
    });
  },

  updateView: function(){
    var dashboard = this;

    dashboard.$lists.html("");
    dashboard.lists.forEach(function(list){
      dashboard.$lists.append(list.render());
    });
  },

  addList: function(name){
    var list = TodoList.create({
      name: name
    });
    
    this.lists.push(list);
    this.sort();
    list.setDashboard(this);
    this.updateView();
    return list;
  },

  removeList: function(list){
    var index = this.lists.indexOf(list);
    if (index > -1){
      this.lists.splice(index, 1);
      this.updateView();
    }
  },

  sort: function(){
    this.lists = this.lists.sort(function(a, b){
      return a.name > b.name ? 1 : -1;
    });
  },

  getOpenItems: function(){
    return this.lists.map(function(list){
      return {
        name: list.name,
        items: list.items.filter(function(item){
          return !item.isCompleted;
        })
      }
    });
  }
});